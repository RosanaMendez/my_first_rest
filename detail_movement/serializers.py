from rest_framework import serializers
from detail_movement.models import DetailMovement

class DetailMovementSerializer(serializers.ModelSerializer):
    class Meta:
        model = DetailMovement
        fields = ['head', 'registration_date', 'description', 
        'credit', 'accounting_code_c', 'accounting_name_c', 
        'debit', 'accounting_code_d', 'accounting_name_d']