from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from detail_movement import views

urlpatterns = [
    path('detail_movement/', views.DetailMovementList.as_view()),
    path('detail_movement/<int:pk>/', views.DetailMovementDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)

