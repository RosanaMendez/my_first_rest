from django.db import models

class DetailMovement(models.Model):
    head = models.CharField(max_length=100, blank=True, default='')
    registration_date = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=100, blank=True, default='')
    credit = models.IntegerField()
    accounting_code_c = models.CharField(max_length=100, blank=True, default='')
    accounting_name_c = models.CharField(max_length=100, blank=True, default='')
    debit = models.IntegerField()
    accounting_code_d = models.CharField(max_length=100, blank=True, default='')
    accounting_name_d = models.CharField(max_length=100, blank=True, default='')

    class Meta:
        ordering = ['registration_date']
