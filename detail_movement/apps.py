from django.apps import AppConfig


class DetailMovementConfig(AppConfig):
    name = 'detail_movement'
