from detail_movement.models import DetailMovement
from detail_movement.serializers import DetailMovementSerializer
from rest_framework import generics

class DetailMovementList(generics.ListCreateAPIView):
    queryset = DetailMovement.objects.all()
    serializer_class = DetailMovementSerializer

class DetailMovementDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = DetailMovement.objects.all()
    serializer_class = DetailMovementSerializer
